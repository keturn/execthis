use std::{fs, io, path};

// The thought process was:
//
// 1. We have several options for where to send a thing. That should be an enum so the handler
//    knows it catches all the cases!
//
// 2. Enumerated items can be instanced and hold data, so that's where we should hold attributes
//    like name and clobberin'.
//
// 3. Having one open() method for all those cases is too much in one function and too much
//    nested inside the match arms, these things should implement their own open methods.
//
// but it's exploded in to a lot of lines of code.
//
// Would be better if we had that thing that makes enum variants more like types:
// https://github.com/rust-lang/rfcs/pull/2593
//
// This might help?  https://crates.io/crates/enum_dispatch
//
// Or maybe it'd be easier for the structs to have an enum field instead of being
// *in* the enum.

const PREFIX: &str = "exec-";
const SUFFIX: &str = ".argv";

type PathAndWriter = (path::PathBuf, fs::File);

pub trait Openable {
    fn open(&self) -> io::Result<PathAndWriter>;
}

#[derive(Debug)]
pub struct NamedFile<'a> {
    pub path: &'a path::Path,
}

#[derive(Debug)]
pub struct NewTempFile;

#[derive(Debug)]
pub struct NewInDirectory<'a> {
    pub path: &'a path::Path,
}

#[derive(Debug)]
pub enum DestinationType<'a> {
    NamedFile(NamedFile<'a>),
    NewTempFile(NewTempFile),
    NewInDirectory(NewInDirectory<'a>),
}

impl<'a> DestinationType<'a> {
    pub fn open(&'a self) -> io::Result<PathAndWriter> {
        let openable: &'a dyn Openable = match self {
            DestinationType::NamedFile(x) => x,
            DestinationType::NewTempFile(x) => x,
            DestinationType::NewInDirectory(x) => x,
        };
        openable.open()
    }
}

// TODO: these could probably be macro'd up?
impl<'a> From<NamedFile<'a>> for DestinationType<'a> {
    #[rustfmt::skip]
    fn from(item: NamedFile<'a>) -> Self { Self::NamedFile(item) }
}

impl From<NewTempFile> for DestinationType<'_> {
    #[rustfmt::skip]
    fn from(item: NewTempFile) -> Self { Self::NewTempFile(item) }
}

impl<'a> From<NewInDirectory<'a>> for DestinationType<'a> {
    #[rustfmt::skip]
    fn from(item: NewInDirectory<'a>) -> Self { Self::NewInDirectory(item) }
}

impl Openable for NamedFile<'_> {
    fn open(&self) -> io::Result<PathAndWriter> {
        let outfile = fs::OpenOptions::new()
            .write(true)
            .create_new(true)
            .open(&self.path)?;
        Ok((self.path.to_owned(), outfile))
    }
}

impl Openable for NewTempFile {
    fn open(&self) -> io::Result<PathAndWriter> {
        let new_tempfile = temp_builder().tempfile()?;
        let (outfile, p) = new_tempfile.keep()?;
        Ok((p, outfile))
    }
}

impl Openable for NewInDirectory<'_> {
    fn open(&self) -> io::Result<PathAndWriter> {
        let new_tempfile = temp_builder().tempfile_in(self.path)?;
        let (outfile, p) = new_tempfile.keep()?;
        Ok((p, outfile))
    }
}

fn temp_builder() -> tempfile::Builder<'static, 'static> {
    let mut b = tempfile::Builder::new();
    b.prefix(PREFIX).suffix(SUFFIX);
    b
}
