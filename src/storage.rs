use std::convert::TryInto;
use std::ffi::CString;
use std::fs;
use std::io;
use std::io::Write;
use std::os::unix::ffi::OsStrExt;
use std::os::unix::fs::PermissionsExt;

use nix::sys::stat::Mode;
use rmp::encode;
use rmpv::decode;
use std::io::Read;

type Bytes = Vec<u8>;

const SHEBANG_MAX: usize = 512;
/// Maximum time to look for the end of a #!

pub(crate) fn write_msgpack_array<'a, W, A>(mut output: W, array: A) -> io::Result<()>
where
    W: io::Write,
    A: ExactSizeIterator<Item = &'a Bytes>,
{
    encode::write_array_len(
        &mut output,
        array.len().try_into().expect("array looks too big?"),
    )?;
    for a in array {
        encode::write_bin(&mut output, a)?;
    }
    Ok(())
}

pub(crate) fn write_shebang(writer: &mut fs::File) -> io::Result<()> {
    let exe = std::env::current_exe()?;
    writer.write_all(b"#!")?;
    writer.write_all(exe.as_os_str().as_bytes())?;
    writer.write_all(b"\n")?;
    let mut p = writer.metadata()?.permissions();
    p.set_mode(Mode::S_IRWXU.bits());
    writer.set_permissions(p)?;
    Ok(())
}

pub(crate) fn load_args<F>(mut infile: &mut F) -> io::Result<Vec<CString>>
where
    F: io::Read + io::Seek,
{
    skip_shebang(infile)?;

    let pos = tell(infile);
    let stuff = decode::read_value(&mut infile).unwrap();
    let args: Vec<CString> = if let rmpv::Value::Array(arg_values) = stuff {
        let args = arg_values
            .iter()
            .map(|v| CString::new(v.as_slice().expect("item wasn't bytes")).expect("null in args"));
        args.collect()
    } else {
        infile.seek(pos)?;
        let marker = rmp::decode::read_marker(&mut infile).unwrap();
        panic!("Expected Array, got {:#?}", marker);
    };
    Ok(args)
}

fn skip_shebang<R: io::Seek + io::Read>(infile: &mut R) -> io::Result<()> {
    let start_pos = tell(infile);
    let mut magic: [u8; 2] = [0; 2];
    infile.read_exact(&mut magic)?; // if we can't read two bytes, there's not a message anyway
    if &magic == b"#!" {
        for (i, chr) in infile.bytes().enumerate() {
            let chr = chr?;
            if chr == b'\n' {
                break;
            }
            if i >= SHEBANG_MAX {
                return Err(io::Error::new(
                    io::ErrorKind::InvalidData,
                    "Input started with shebang but couldn't find line end.",
                ));
            }
        }
    } else {
        // Didn't match, return to original position.
        infile.seek(start_pos)?;
    }
    Ok(())
}

fn tell(infile: &mut impl io::Seek) -> io::SeekFrom {
    io::SeekFrom::Start(
        infile
            .seek(io::SeekFrom::Current(0))
            .expect("input not seekable"),
    )
}
