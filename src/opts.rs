use std::borrow::Cow;
use std::ffi::{OsStr, OsString};
use std::os::unix::ffi::OsStrExt;
use std::path;

use structopt::clap::{AppSettings, ArgGroup};
use structopt::StructOpt;

use crate::destinations;
use crate::destinations::DestinationType;

type Bytes = Vec<u8>;
type DirPathBuf = path::PathBuf;

// structopt todo:
//  - in save, why is positional <command> before option group?
//      DontCollapseArgsInUsage?

#[derive(Debug, StructOpt)]
#[structopt(
    setting=AppSettings::TrailingVarArg,
    group=ArgGroup::with_name("destination").required(true))
]
pub(crate) struct SaveOpt {
    #[structopt(
        long = "output",
        short = "o",
        value_name = "FILE",
        parse(from_os_str),
        group = "destination"
    )]
    /// write command and args to this file
    to_file: Option<path::PathBuf>,

    #[structopt(long = "tempfile", short = "t", group = "destination")]
    /// write to a new temporary file
    to_tempfile: bool,

    #[structopt(
        long = "dir",
        short = "d",
        value_name = "DIRECTORY",
        parse(try_from_os_str=parse_as_existing_directory),
        group = "destination"
    )]
    /// write a new file in this directory
    to_dir: Option<DirPathBuf>,

    #[structopt(long, short = "S", aliases(&["sha-bang", "hashbang"]))]
    /// include #! to make the file directly executable
    pub(crate) shebang: bool,

    #[structopt(required=true, value_name="command [args]", parse(from_os_str=bytes_from_os_str))]
    /// command to execute followed by its args
    pub(crate) args: Vec<Bytes>,
}

impl SaveOpt {
    #[rustfmt::skip]  // use_small_heuristics=Max
    pub(crate) fn destination_type(&self) -> DestinationType {
        // structopt doesn't reflect groups, so express as enum after the fact
        match self {
            SaveOpt { to_file: Some(path), .. } => destinations::NamedFile{path}.into(),
            SaveOpt { to_tempfile: true, .. } => destinations::NewTempFile {}.into(),
            SaveOpt { to_dir: Some(path), .. } => destinations::NewInDirectory{path}.into(),
            _ => unreachable!(), // enforced by the required ArgGroup
        }
    }

    pub(crate) fn command_display(&self) -> Cow<str> {
        OsStr::from_bytes(self.args.get(0).expect("no item in required args!")).to_string_lossy()
    }
}

#[derive(Debug, StructOpt)]
pub(crate) enum SubCommand {
    #[structopt(name = "--save", visible_alias = "-s")]
    /// save this command its arguments to a file
    Save(SaveOpt),
}

#[derive(Debug, StructOpt)]
pub(crate) struct LoadOpt {
    // SubcommandsNegateReqs needs some extra help in structopt:
    // https://github.com/TeXitoi/structopt/issues/124
    #[structopt(value_name = "INPUT", parse(from_os_str), required_unless = "--save")]
    /// load commands and args from this file
    pub input: Option<path::PathBuf>,
}

#[derive(Debug, StructOpt)]
#[structopt(
    global_settings(&[
        AppSettings::ArgRequiredElseHelp,
        AppSettings::ColoredHelp,
        AppSettings::DeriveDisplayOrder,
        AppSettings::UnifiedHelpMessage,
    ]),
    settings(&[
        AppSettings::DisableHelpSubcommand,
        AppSettings::VersionlessSubcommands,
        AppSettings::ArgsNegateSubcommands,
        AppSettings::SubcommandsNegateReqs,
    ]),
)]
pub(crate) struct Opt {
    #[structopt(subcommand)]
    pub subcommand: Option<SubCommand>,

    #[structopt(flatten)]
    pub load_opt: LoadOpt,
}

fn bytes_from_os_str(s: &OsStr) -> Bytes {
    s.as_bytes().to_vec()
}

fn parse_as_existing_directory(s: &OsStr) -> Result<DirPathBuf, OsString> {
    // It's only worth validating up front here if it produces clearer error messages.
    // (Since no work happens before trying to open this path, there's no resources saved by
    // checking it early.)
    // Letting clap display the IO errors as they bubble up might be okay, but the current
    // version eats some messages: https://github.com/clap-rs/clap/issues/1614

    let p = path::PathBuf::from(s);
    if !p.exists() {
        return Err(format!("Directory does not exist at {}", s.to_string_lossy()).into());
    };
    let metadata = p.metadata().expect("metadata fail for existing directory");
    if !metadata.is_dir() {
        return Err(format!("This is not a directory: {}", s.to_string_lossy()).into());
    }
    Ok(p)
}
