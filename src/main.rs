use std::ffi::{CStr, CString};
use std::fs;
use std::io;
use std::io::Write;
use std::os::unix::ffi::OsStrExt;

use nix::unistd;
use structopt::{clap, StructOpt};

use opts::{LoadOpt, Opt, SaveOpt, SubCommand};
use storage::{load_args, write_msgpack_array, write_shebang};

#[cfg(not(unix))]
compile_error!("This probably only makes sense in Unix!");

mod destinations;
mod opts;
mod storage;

// use core::any::type_name;
// fn type_name_of<T>(_: &T) -> &str { type_name::<T>() }

// Saving:
// - what to save
//   - PATH (from env or as given)
//   - ENV
// - guards:
//   - confirm command exists and is executable
//
// about PATH:
//   - does it make any sense to save it for execvp?
//     or better to resolve it now?
//
//  --dry-run
//  --verbose

fn main() -> Result<(), clap::Error> {
    let opt = Opt::from_args();
    // println!("{:#?}", Opt::clap().get_matches());

    if let Some(SubCommand::Save(save_opt)) = &opt.subcommand {
        do_save(save_opt)?;
    } else {
        do_load(&opt.load_opt)?;
    }

    Ok(())
}

fn do_save(save_opt: &SaveOpt) -> io::Result<()> {
    let dest = save_opt.destination_type();
    let (dest_path, mut writer) = dest.open()?;
    eprintln!("Writing to {:#?}", dest_path);
    eprintln!(
        "`{}` plus {} args",
        &save_opt.command_display(),
        &save_opt.args.len() - 1
    );
    if save_opt.shebang {
        write_shebang(&mut writer)?;
    }
    write_msgpack_array(writer, save_opt.args.iter())?;
    io::stdout().write_all(dest_path.as_os_str().as_bytes())?;
    Ok(())
}

fn do_load(load_opt: &LoadOpt) -> Result<(), io::Error> {
    let load_path = load_opt.input.as_ref().expect("no input to load");

    let args: Vec<CString> = {
        let mut infile = fs::OpenOptions::new().read(true).open(load_path)?;
        load_args(&mut infile)?
    };

    let command: &CString = args.get(0).expect("loaded args were empty");
    eprintln!("command: {}", command.to_string_lossy());

    let arg_buf: Vec<&CStr> = args.iter().map(CString::as_c_str).collect();
    if let Err(exec_error) = unistd::execvp(command, arg_buf.as_slice()) {
        eprintln!(
            "Failed to execute '{}': {}",
            command.to_string_lossy(),
            exec_error
        );
        panic!(); // return Err(exec_error);
    }

    unreachable!("if exec succeeded you would never see this");
}
