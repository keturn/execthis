execthis
========

Save command-line arguments for later execution.

<pre>
<b>## Saving</b>
$ execthis --save --output=<i>day date +'Today is %A'</i>

<b>## Load and execute</b>
$ execthis <i>./day</i>
Today is Wednesday
</pre>

The result is like a one-command shell script. But unlike writing to a script to be evaluated by `sh`, once inputs are written by `execthis` they will not undergo any further expansion, splitting, or interpretation.

* `$` `#` `~` `*` `?`  `<`  `>` `|` `&`  `;` `!` <code>&#92;</code> `[]` `{}` `()` `""` `''` <code>&#96;&#96;</code>

The defining feature of this program is it has _none of those features._

This is probably not so useful for direct invocation — any arguments you can type for a `execthis --save` command line you could just as well type into a script yourself — but comes up in scripts that have some variables you want another process to have later, unmodified.

Example:

```shell script
#!/bin/sh
mkdir -p --mode=700 ~/.ssh_commands.d/
CMD=$( execthis --save --dir ~/.ssh_commands.d/ --shebang "$@" )
printf %s "restrict,command=\"$CMD\" " |
    cat - some-key.pub >> ~/.ssh/authorized_keys2
```

Options used above are:

* **--dir _\<DIRECTORY>_** : execthis will create a new file in this directory (`~/.ssh_commands.d/`). It prints the name of this output file to stdout, which the shell captures with `$()`.
* **--shebang** : the file is set executable so it can be invoked directly without explicitly calling `execthis` to load it.

Another example:
```shell script
#!/bin/sh
CMD=$( execthis --save --tempfile --shebang "$@" )
socat TCP-L:7777 EXEC:"$CMD"
rm "$CMD"
```

* **--tempfile** : similar to `--dir`, creates a new file in the default temp directory (i.e. `$TMPDIR`).


Alternatives
------------

This came up because I couldn't find a way to do this in POSIX `sh` (or sed or awk) without an undecipherable pile of leaning toothpicks, but your platform may offer other ways of reliably escaping input for a shell:

* `printf` offers the `%q` formatter for escaping strings in GNU coreutils 8.25 (released 2016)
* `bash` has <code>${<i>FOO</i><b>@Q</b>}</code> to escape the content of a variable <code><i>FOO</i></code> as of version 4.4 (released 2016)
* `zsh` has <code>${<b>(q)</b><i>FOO</i>}</code> to escape the content of a variable <code><i>FOO</i></code> since before version 3.0 (as far back as the release notes go)


Implementation Details
----------------------

### File Format

execthis stores arguments in [MessagePack](https://msgpack.org) format. This allows us to keep arrays of arbitrary data with *no escape characters.* No escaping means we can't mess up the escaping. (Hopefully.)

It's not a format that's friendly to modification by text editor, but encoders and decoders are readily available for numerous languages.

When saving with the `--shebang` option, a line beginning with `#!` and ending with newline precedes the MessagePack data. execthis ignores this line on load. (`#!` is not the start of any MessagePack array that execthis can write, meaning there's no risk of confusing it for the data.)


### Limitations

Commands and arguments may not contain null bytes. This is inherent in the use of null-terminated strings by the operating system's syscall interface.

This was written with Un*x systems in mind; Windows doesn't have `exec` and I'm not sure how this could be useful there.